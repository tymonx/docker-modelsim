# Copyright 2020 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.download-modelsim-quartus:
    extends: .download
    variables:
        FREETYPE_URL: http://download.savannah.gnu.org/releases/freetype
        FREETYPE_MD5: dbadce8f0c5e70a0b7c51eadf2dd9394
        FREETYPE_VERSION: 2.4.7
        FONTCONFIG_URL: https://www.freedesktop.org/software/fontconfig/release
        FONTCONFIG_MD5: 29105662c7d319720e0088a0ac53f494
        FONTCONFIG_VERSION: 2.12.4
        QUARTUS_URL: http://download.altera.com/akdlm/software/acdsinst
    before_script:
        - |-
            # Compose URL path from CI job name for download
            function add_url {
                DOWNLOAD_URL="$DOWNLOAD_URL $1"
            }

            function add_md5 {
                DOWNLOAD_MD5="$DOWNLOAD_MD5 $1"
            }

            URL="$QUARTUS_URL"

            # Extract application version from CI job name
            VERSION=$( \
                echo "$CI_JOB_NAME" | \
                sed -n -r 's/^(.*-)?([0-9.]+)(-.*)?$/\2/p' \
            )

            # Extract major version number
            MAJOR=$(echo "$VERSION" | cut -d '.' -f 1)
            MAJOR=${MAJOR:-0}

            # Extract minor version number
            MINOR=$(echo "$VERSION" | cut -d '.' -f 2)
            MINOR=${MINOR:-0}

            # Extract patch version number
            PATCH=$(echo "$VERSION" | cut -d '.' -f 3)
            PATCH=${PATCH:-0}

            # Extract build version number
            BUILD=$(echo "$VERSION" | cut -d '.' -f 4)
            BUILD=${BUILD:-0}

            # Compose base URL path
            URL="${URL}/${MAJOR}.${MINOR}"

            # If 'lite' or 'standard' id name is present in CI job name,
            # add 'std' sub name suffix to URL path
            URL="${URL}$( \
                echo "$CI_JOB_NAME" | \
                tr '[:upper:]' '[:lower:]' | \
                sed -n -r 's/^(.*-)?(lite|standard)(-.*)?$/std/p' \
            )"

            # Add build path to URL path
            URL="${URL}/${BUILD}/ib_installers/ModelSim"

            # If 'pro' id name is present in CI job name, add 'Pro' sub name to
            # URL path
            URL="${URL}$( \
                echo "$CI_JOB_NAME" | \
                tr '[:upper:]' '[:lower:]' | \
                sed -n -r 's/^(.*-)?pro(-.*)?$/Pro/p' \
            )"

            # Add the final file name to a composed URL path
            URL="${URL}Setup-${MAJOR}.${MINOR}.${PATCH}.${BUILD}-linux.run"

            # Add second URL path for Quartus Pro ModelSim
            case "$URL" in
                *ProSetup*)
                    URL="${URL} $(echo ${URL} | sed 's/Setup-/Setup-part2-/')"
                    ;;
            esac

            add_url "$URL"
            add_url "${FREETYPE_URL}/freetype-${FREETYPE_VERSION}.tar.bz2"
            add_url "${FONTCONFIG_URL}/fontconfig-${FONTCONFIG_VERSION}.tar.bz2"

            add_md5 "$FREETYPE_MD5"
            add_md5 "$FONTCONFIG_MD5"
